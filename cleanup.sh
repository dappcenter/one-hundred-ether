#!/usr/bin/env sh

STACK=${STACK_NAME:-one-hundred-ether-wall}-${STACK_ENV:-dev}

# empty bucket before stack removal to prevent errors
aws s3 rm s3://${STACK}-static --recursive \
    && echo " S3 bucket successfully emptied."
# deleting stack
aws cloudformation delete-stack --stack-name ${STACK} > /dev/null \
    && echo "Stack was successfully deleted."
