const HundredEtherWall = artifacts.require("./HundredEtherWall.sol");

contract('HundredEtherWall', (accounts) => {
  const owner = accounts[0];
  const pixelPrice = 80000000000000;
  const pixelsPerCell = 625;
  const cellPrice = pixelPrice * pixelsPerCell;

  //test if contractOwner was set correct by constructor
  it("Contract owner should be you (account[0])", () => {
    let HEW;
    console.log("Attribute tests");

    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        return HEW.contractOwner.call();
      }))
      .then((co => {
        assert.equal(owner, co);
      }))
  });

  //test if attributes are correct
  it("Should have the correct pixel price and pixel size", () => {
    let HEW;
    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        return HEW.pixelPrice.call();
      }))
      .then((pp => {
        assert.equal(pixelPrice, pp.toNumber());
        return HEW.pixelsPerCell.call();
      }))
      .then((ppc => {
        assert.equal(pixelsPerCell, ppc.toNumber());
      }))
  });

  //test if price > 0 on buy()
  it("Should cost a buyer more than 0 ether to actually buy something", () => {
    let HEW;
    console.log("Buy Ad tests");

    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        return HEW.buy(0, 0, 25, 25, "", "", "", { value: 0, from: accounts[1] });
      }))
      .then(( () => {
        assert.fail();
      }))
      .catch((error => {
        assert(error.message.includes("revert"));
      }))
  });

  //test if possible to buy 2 differend ads
  it("Should be possible to buy ads on different spots", () => {
    let HEW;
    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        return HEW.buy(0, 0, 25, 25, "", "", "", { value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.buy(25, 25, 25, 25, "", "", "", { value: cellPrice, from: accounts[2] });
      }))
      .then(( () => {
        assert(true);
      }))
  });

  //test if ads can overlap
  it("Should not be possible to buy ads on the same spot", () => {
    let HEW;
    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        return HEW.buy(75, 75, 25, 25, "", "", "", { value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.buy(75, 75, 25, 25, "", "", "", { value: cellPrice, from: accounts[2] });
      }))
      .then(( () => {
        assert.fail();
      }))
      .catch((error => {
        assert(error.message.includes("revert"));
      }))
  });

  //test if the right ammount of ads are returned
  it("Should be possible to get the amount of ads in the array of the contract", () => {
    let HEW;
    console.log("Get ads test");

    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        return HEW.buy(0, 0, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.buy(25, 25, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.buy(50, 50, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.buy(75, 75, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.getAds();
      }))
      .then(( ads => {
        assert.equal(4, ads);
      }))
  });

  //test if purchased ad can be updated with same account
  it("Should be possible to buy and update an ad with the same account", () => {
    let HEW;
    console.log("Update Ad tests");

    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        return HEW.buy(0, 0, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.update(0, "title", "link", "image", {from: accounts[1] });
      }))
      .then(( () => {
        return HEW.ads.call(0);
      }))
      .then(( ad => {
        assert.equal(accounts[1], ad[0]);
        assert.equal(0, ad[1].toNumber());
        assert.equal(0, ad[2].toNumber());
        assert.equal(25, ad[3].toNumber());
        assert.equal(25, ad[4].toNumber());
        assert.equal("title", ad[5]);
        assert.equal("link", ad[6]);
        assert.equal("image", ad[7]);
      }))
  });

  //test if purchased ad can be updated with different account
  it("Should not be possible to buy an ad and update it with a different account", () => {
    let HEW;

    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        return HEW.buy(0, 0, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.update(0, "title", "link", "image", {from: accounts[2] });
      }))
      .then(( () => {
        assert.fail();
      }))
      .catch((error => {
        assert(error.message.includes("revert"));
      }))
  });

  //test if the contract owner can change another accounts ad (in case of unwanted NSFW content)
  it("Should be possible to update any ad with the contract owners account", () => {
    let HEW;

    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        return HEW.buy(0, 0, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.update(0, "title", "link", "image", {from: accounts[0] });
      }))
      .then(( () => {
        return HEW.ads.call(0);
      }))
      .then(( ad => {
        assert.equal(accounts[1], ad[0]);
        assert.equal(0, ad[1].toNumber());
        assert.equal(0, ad[2].toNumber());
        assert.equal(25, ad[3].toNumber());
        assert.equal(25, ad[4].toNumber());
        assert.equal("title", ad[5]);
        assert.equal("link", ad[6]);
        assert.equal("image", ad[7]);
      }))
  });

  //test if the contract owner can withdraw
  it("Should be possible for the contract owner to withdraw the funds", () => {
    let HEW;
    let oldbalance;
    let newbalance;
    console.log("Withdraw tests");

    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        oldbalance = web3.eth.getBalance(owner);
      }))
      .then(( () => {
        return HEW.buy(0, 0, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.withdraw({from: accounts[0]});
      }))
      .then(( () => {
        newbalance = web3.eth.getBalance(owner);
      }))
      .then(( () => {
        assert(newbalance > oldbalance);
      }))
  });

  //test if any account can withdraw
  it("Should not be possible for any other account to withdraw the funds", () => {
    let HEW;
    let oldbalance;
    let newbalance;

    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
        oldbalance = web3.eth.getBalance(owner)
      }))
      .then(( () => {
        return HEW.buy(0, 0, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.withdraw({from: accounts[1]});
      }))
      .then(( () => {
        newbalance = web3.eth.getBalance(owner);
      }))
      .then(( () => {
        assert(newbalance > oldbalance);
      }))
      .catch((error => {
        assert(error.message.includes("revert"));
      }))
  });

  //test if an ad can be sold and sale-cancled at the marketplace
  it("Should be possible to sell and cancel-sale an ad at the marketplace", () => {
    let HEW;
    console.log("Marketplace tests");

    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
      }))
      .then(( () => {
        return HEW.buy(0, 0, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        return HEW.ads.call(0);
      }))
      .then(( ad => {
        assert.equal(false, ad[8]);
      }))
      .then(( () => {
        return HEW.setSale(0, true, 800000000000000, {from: accounts[1] });
      }))
      .then(( () => {
        return HEW.ads.call(0);
      }))
      .then(( ad => {
        assert.equal(true, ad[8]);
      }))
      .then(( () => {
        return HEW.setSale(0, false, 800000000000000, {from: accounts[1] });
      }))
      .then(( () => {
        return HEW.ads.call(0);
      }))
      .then(( ad => {
        assert.equal(false, ad[8]);
      }))
  });

  //Test if an ad can be bought on the marketplace
  it("Should be possible to buy an ad from the marketplace", () => {
    let HEW;
    let oldbalance;
    let newbalance;

    return HundredEtherWall.new()
      .then((instance => {
        HEW = instance;
      }))
      .then(( () => {
        return HEW.buy(0, 0, 25, 25, "", "", "", {value: cellPrice, from: accounts[1] });
      }))
      .then(( () => {
        oldbalance = web3.eth.getBalance(accounts[1])
      }))
      .then(( () => {
        return HEW.ads.call(0);
      }))
      .then(( ad => {
        assert.equal(false, ad[8]);
      }))
      .then(( () => {
        return HEW.setSale(0, true, 800000000000000, {from: accounts[1] });
      }))
      .then(( () => {
        return HEW.ads.call(0);
      }))
      .then(( ad => {
        assert.equal(true, ad[8]);
      }))
      .then(( () => {
        return HEW.marketBuy(0, {from: accounts[2] });
      }))
      .then(( () => {
        return HEW.ads.call(0);
      }))
      .then(( ad => {
        assert.equal(false, ad[8]);
      }))
      .then(( () => {
        newbalance = web3.eth.getBalance(accounts[1])
      }))
      .then(( () => {
        assert(newbalance > oldbalance);
      }))
      .catch((error => {
        assert(error.message.includes("revert"));
      }))
  });

});
