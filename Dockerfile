# docker build -t ubuntu1604py36
FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y software-properties-common vim
RUN add-apt-repository ppa:jonathonf/python-3.6
RUN apt-get update

RUN apt-get install -y build-essential python3.6 python3.6-dev python3-pip python3.6-venv python3-distutils
RUN apt-get install -y git

# update pip
RUN python3.6 -m pip install pip --upgrade
RUN python3.6 -m pip install wheel

# install aws cli
RUN pip install awscli

# install curl
RUN apt-get install curl

# install nodejs
#RUN apt-get install -y nodejs
#RUN apt-get install -y npm
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs
RUN apt-get install -y build-essential

RUN npm install -g truffle
RUN npm install -g ganache-cli
RUN npm install -g run-with-testrpc
