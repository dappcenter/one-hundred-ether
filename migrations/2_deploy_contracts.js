var HundredEtherWall = artifacts.require("./HundredEtherWall.sol");

// module.exports = function(deployer) {
//   deployer.deploy(HundredEtherWall);
// };

module.exports = function(deployer) {
  deployer.deploy(HundredEtherWall)
  .then(() => HundredEtherWall.deployed())
  .then(hundredEtherWall => new Promise(resolve => setTimeout(() => resolve(hundredEtherWall), 6000)));
};

// module.exports = function (deployer) {
//   deployer.then(function() {
//      return new Promise(function(accept, reject){
//        deployer
//          .deploy(HundredEtherWall)
//          .on("confirmation", (number, receipt) => {
//             if (number === 10) accept(receipt);
//          })
//          .catch(reject)
//       });
//    })
// };
