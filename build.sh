#!/usr/bin/env sh

BUCKET=${DEPLOYMENT_BUCKET:-one-hundred-ether-wall}
STACK_ENV=${STACK_ENV:-dev}

# Delete build contents
rm -rf build_webpack \
  && echo "Removing old build"

# Build client
echo "Building client..."
npm run build \
  && echo "Successfully built client."

# Create S3 Bucket
aws cloudformation package \
    --template-file deployment.yaml \
    --s3-bucket ${BUCKET}${STACK_ENV}-static \
    --output-template-file build_webpack/deployment.yaml \
    > /dev/null \
    && echo "Package was successfully built."
