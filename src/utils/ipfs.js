//using the infura.io node, otherwise ipfs requires you to run a //daemon on your own computer/server.
// import IpfsApi from 'ipfs-api';
// const ipfs = new IpfsApi({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' });

var ipfs = window.IpfsApi('ipfs.infura.io', '5001', {protocol: 'https'})

//run with local daemon
// const ipfsApi = require(‘ipfs-api’);
// const ipfs = new ipfsApi(‘localhost’, ‘5001’, {protocol:‘http’});
export default ipfs;
