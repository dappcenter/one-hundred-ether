import Alert from 'react-s-alert';

const handleErrorMessage = (message) => {
  Alert.error(message, {
    position: 'top',
    effect: 'jelly',
    timeout: 5000,
  });
}

const handleSuccessMessage = (message) => {
  Alert.success(message, {
    position: 'top',
    effect: 'jelly',
    timeout: 5000,
  });
}

function handleCloseAll(e) {
  e.preventDefault();
  Alert.closeAll();
}

export {handleErrorMessage, handleSuccessMessage, handleCloseAll}
