import React from "react"
import '../styles/header.css';
import * as strings from '../config/strings';
import {Button} from 'react-bootstrap';
import InfoModal from './InfoModal'

class Header extends React.Component {

  render() {
    return (
      <div className="total">
        <div className="infobutton">
          <InfoModal soldBlocks={this.props.soldBlocks}/>
          <Button
            className="btn-warning withdrawButton"
            style={this.props.style}
            onClick={this.props.withdraw}
          >
          <i className="fas fa-dollar-sign"></i>
          </Button>
        </div>
        <div className="left">
          <p><strong>{strings.headerTitleLeft}</strong><br />{strings.headerTextLeft}</p>
        </div>
        <div className="mid">
          <h1>{strings.headerTitleMid}</h1>
          <a href={this.props.networkLink} target="blank" className="networkLink">
            <div className="transition">
              <h4>{strings.headerNetwork + this.props.network}</h4>
            </div>
          </a>
        </div>
        <div className="right">
          <p><strong>{strings.headerTitleRight}</strong><br />{strings.headerTextRight}</p>
        </div>
      </div>
    );
  }
}

export default Header;
