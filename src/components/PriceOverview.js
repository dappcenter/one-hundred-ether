import React from 'react';
import '../styles/modal.css';
import {Button} from 'react-bootstrap';
import {calculatePos, calculateSize} from '../utils/gridCalculator';
import {blockSize, blockPrize} from '../config/config';
import * as strings from '../config/strings';

class PriceOverview extends React.Component {

	render() {
    let selected = this.props.selectedItems;
    let titleText = selected.length === 1 ?
    strings.priceOverViewTitle1 + selected.length + strings.priceOverViewTitle2 +
		" (" + calculateSize(this.props.selectedItems)[0] + " x " + calculateSize(this.props.selectedItems)[1] + ")" :
    strings.priceOverViewTitle1 + selected.length + strings.priceOverViewTitle3 +
		" (" + calculateSize(this.props.selectedItems)[0] + " x " + calculateSize(this.props.selectedItems)[1] + ")";

    let pixelCalc = blockSize * selected.length;
    let priceCalc = (selected.length % 2) ? (blockPrize * selected.length).toFixed(2) : (blockPrize * selected.length).toFixed(1);

		return (
			<div>
				<div className='h2Wrapper'>
					<h2 className="modalH2">{titleText}</h2>
				</div>
				<h3>{strings.priceOverviewTableTitle}</h3>
					<table>
						<tbody>
							<tr>
								<th className="noBorder"></th>
								<th>{strings.overviewUnit}</th>
								<th>{strings.overviewSelection}</th>
							</tr>
							<tr>
								<th>{strings.overviewBlocks}</th>
								<td>1</td>
								<td>{selected.length}</td>
							</tr>
							<tr>
								<th>{strings.overviewPixelsCaps}</th>
								<td>25 x 25 = {blockSize + strings.overviewPixels}</td>
								<td>{calculateSize(this.props.selectedItems)[0]} x {calculateSize(this.props.selectedItems)[1]} = {pixelCalc + strings.overviewPixels}</td>
							</tr>
							<tr>
								<th>{strings.overviewPosition}</th>
								<td>-</td>
								<td>{strings.overviewColumn + (calculatePos(0, this.props.selectedItems)[0] + 1) + strings.overviewRow + (calculatePos(0, this.props.selectedItems)[1] + 1)}</td>
							</tr>
							<tr>
								<th>Price</th>
								<td>{blockPrize} ETH</td>
								<td>{priceCalc} ETH</td>
							</tr>
							<tr>
								<th>{strings.overviewGasfee}</th>
								<td>{strings.overviewEst}</td>
								<td>{strings.overviewSuggest}</td>
							</tr>
						</tbody>
					</table>

					<div className="pull-right">
						<Button bsStyle="primary" className="nextButton" onClick={this.props.onOpenForm}>Next</Button>
					</div>
			</div>
		);
	}
}

export default PriceOverview;
