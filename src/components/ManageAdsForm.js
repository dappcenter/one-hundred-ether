import React from 'react';
import {DropdownButton, MenuItem} from 'react-bootstrap';
import '../styles/manage-ads.css';
import {Button} from 'react-bootstrap';
import ipfs from '../utils/ipfs';
import { RingLoader } from 'react-spinners';
import logo from '../../public/images/logo.jpg';
import * as strings from '../config/strings';

class ManageAdsForm extends React.Component {

  constructor (props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.initiateUpdate = this.initiateUpdate.bind(this);
    this.initiateActivation = this.initiateActivation.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleLinkChange = this.handleLinkChange.bind(this);
    this.validateUrl = this.validateUrl.bind(this);
    this.captureFile = this.captureFile.bind(this);
    this.convertToBuffer = this.convertToBuffer.bind(this);

    this.state = {
      image: null,
      title: "",
      link: "",
      ipfsHash: "",
      active: true,
      linkCorrect: false,
      imageCorrect: false,
      menuItems: [],
      buffer: '',
      loading: false,
      fileSize: null,
      disableManageAds: false,
      style: null,
      activeStyle: null,
    };
  }

  render(){
    if (this.props.web3) {
      return (
        <div className="manageAdsContainer" style={this.state.style}>
          <div className='sweet-loading'>
            <RingLoader
              color={'#65cdf1'}
              loading={this.state.loading}
            />
          </div>
          <div className="info">
            <h1>{strings.manageAdsTitle}</h1>
            <p>{strings.manageAdsExp} <br /> <br />
              {strings.manageAdsNote}<br />
              {strings.formExp2}<br />
              {strings.formExp3}<br />
              {strings.formExp4}
              </p>
          </div>
          <form className="form-horizontal">
  					<div className="form-group">

              <label className="col-sm-2 control-label" htmlFor="Ad">{strings.manageAdsAdLabel}</label>
              <div className="col-sm-10">
                <DropdownButton
                  bsSize="large"
                  title={this.state.active ? this.state.title.substring(0, 40) : this.state.title.substring(0, 40) + strings.manageAdsInactiveDrop}
                  id="dropdown-size-large"
                  className="dropdown"
                >
                  {this.loadMenuItems()}
                </DropdownButton>
              </div>

  						<label className="col-sm-2 control-label" htmlFor="title">{strings.manageAdsTitleLabel}</label>
  						<div className="col-sm-10">
  							<input
  								type="text"
  								className="form-control"
  								id="title"
                  value={this.state.title}
                  maxLength="70"
                  onChange={this.handleTitleChange}
  							/>
  						</div>

  						<label className="col-sm-2 control-label" htmlFor="link">{strings.manageAdsLinkLabel}</label>
  						<div className="col-sm-10">
  							<input
  								type="text"
  								className="form-control"
  								id="link"
                  value={this.state.link}
                  onChange={this.handleLinkChange}
  							/>
  						</div>

              <label className="col-sm-2 control-label" htmlFor="image">{strings.manageAdsCurrentImgLabel}</label>
              <div className="col-sm-10">
                <img src={this.state.ipfsHash ? "https://gateway.ipfs.io/ipfs/" + this.state.image.ipfsHash : logo} role="presentation" width="40px" height="40px"/>
              </div>

              <label className="col-sm-2 control-label" htmlFor="image">{strings.manageAdsNewImgLabel}</label>
              <div className="col-sm-10">
                <input type="file" className="custom-file-input" accept="image/*" onChange={this.captureFile}/>
              </div>

              <label className="col-sm-2 control-label" htmlFor="size"></label>
              <div className="col-sm-10">
                <p>
                  File size: {Math.round(this.state.fileSize)} KB {this.state.fileSize > 300 ?
                  strings.fileSizeTooLarge : strings.fileSizeWithinLimit}
                </p>
              </div>

  					</div>
  				</form>
          <div className="buttonBar">
            <label className="col-sm-2 control-label" htmlFor="submit"></label>
            <Button
              bsStyle="success"
              className='buyButton submit'
              onClick={this.initiateUpdate}
              disabled={!this.state.imageCorrect || !this.state.linkCorrect || this.state.fileSize > 300}
            >
              {strings.manageAdsSave}
            </Button>

            <Button
              bsStyle="default"
              className='activeButton'
              style={this.state.activeStyle}
              onClick={this.initiateActivation}
            >
              {this.state.active ? strings.manageAdsInactive : strings.manageAdsActive}
            </Button>
          </div>
        </div>
      );
    } else {
      return (null);
    }
  }

  loadMenuItems() {
    let menuItems = [];
    for (let i = 0; i < this.props.images.length; i++) {
      for (let x = 0; x < this.props.web3.eth.accounts.length; x++) {
        if (this.props.images[i].owner === this.props.web3.eth.accounts[x] || this.props.contractOwner) {
          menuItems.push(<MenuItem onClick={() => this.onClick(this.props.images[i])} key={i}>{this.props.images[i].active ? this.props.images[i].title
          : this.props.images[i].title + strings.manageAdsInactiveDrop}</MenuItem>);
        }
      }
    }
    return menuItems;
  }

  displayCheck() {
    let selected = false;
    let count = 0;
    for (let i = 0; i < this.props.images.length; i++) {
      for (let x = 0; x < this.props.web3.eth.accounts.length; x++) {
        if (this.props.images[i].owner === this.props.web3.eth.accounts[x] || this.props.contractOwner) {
          count += 1;
          this.props.enabledMessage();
          this.setState({
            style: null,
          });
          if (!selected) {
            this.onClick(this.props.images[i]);
            selected = true;
          }
        }
      }
    }
    if (count === 0) {
      let style = {
        display: "none",
      }
      this.setState({
        style: style,
      });
      this.props.disabledMessage();
    }
  }

  onClick(image) {
    this.setState({
      image: image,
      title: image.title,
      link: image.link,
      ipfsHash: image.ipfsHash,
      active: image.active,
      imageCorrect: true,
      linkCorrect: true,
    });
  }

  initiateUpdate() {
    this.setState({ loading: true });
    if (this.state.buffer) {
      ipfs.add(this.state.buffer, (err, ipfsHash) => {

        if (ipfsHash[0].hash !== this.state.ipfsHash && ipfsHash[0].hash) {
          this.props.update(this.state.title, this.state.link, ipfsHash[0].hash, this.state.image);
          this.setState({
            buffer: null,
            loading: false,
          })
        }
      })
    } else {
        this.props.update(this.state.title, this.state.link, this.state.ipfsHash, this.state.image);
        this.setState({ loading: false });
    }
  }

  initiateActivation() {
    this.props.changeActive(!this.state.image.active, this.state.image);
  }

  handleTitleChange(e) {
    this.setState({
      title: e.target.value,
    });
  }

  handleLinkChange(e) {
    let link = e.target.value;
    this.setState({link: link});
    let validLink = this.validateUrl(link);
    if (validLink) {
      this.setState({linkCorrect: true})
    } else {
        this.setState({linkCorrect: false})
    }
  }

  validateUrl(link) {
    let substring = "https://";
    let substring2 = "http://";

    if (link.includes(substring) || link.includes(substring2)) {
      return true;
    } else {
        return false;
    }
  }

  validateImageExtension(url){
    return(url.match(/\.(jpeg|jpg|gif|png|ico)$/) != null);
  }

  captureFile(e) {
    try {
      e.stopPropagation();
      e.preventDefault();
      const file = e.target.files[0];
      this.setState({fileSize: file.size / 1024 })

      let validExtension = this.validateImageExtension(file.name);

      if (validExtension) {
        this.setState({imageCorrect: true})
      } else {
          this.setState({imageCorrect: false})
      }

      let reader = new window.FileReader();
      reader.readAsArrayBuffer(file);
      reader.onloadend = () => this.convertToBuffer(reader);
    } catch (error) {
      //
    }
  }

  async convertToBuffer(reader) {
     //file is converted to a buffer for upload to IPFS
       const buffer = await Buffer.from(reader.result);
     //set this buffer -using es6 syntax
       this.setState({buffer});
  }


  componentWillMount() {
    this.displayCheck();
    if (!this.props.contractOwner) {
      let style = {
        display: "none",
      }
      this.setState({activeStyle: style});
    }
  }

}

export default ManageAdsForm;
