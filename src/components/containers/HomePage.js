import React from 'react';
import '../../styles/home-page.css';
import SelectableGrid from '../SelectableGrid';
import BuyModal from '../BuyModal';
import ManageAdsForm from '../ManageAdsForm';
import items from '../items';
import {Button} from 'react-bootstrap';
import { Web3Provider } from 'react-web3';
import Contract from 'truffle-contract';
import HundredEtherWallContract from '../../../build/contracts/HundredEtherWall.json'
import getWeb3 from '../../utils/getWeb3';
import Header from '../Header';
import scrollToComponent from 'react-scroll-to-component';
import {handleErrorMessage, handleSuccessMessage, handleCloseAll} from '../Notification';
import {calculateCoords, calculateSize, calculateBlockPos} from '../../utils/gridCalculator';
import {checkNetwork} from '../../utils/network';
import {weiPixelPrice, highGasPrice, midGasPrice,
        lowGasPrice, highGasLimit, lowGasLimit} from '../../config/config';
import * as strings from '../../config/strings';

const buttonText = strings.startSelectButtonText;
const clearText = strings.clearSelectButtonText;
const buyButtonClass = "btn-right";
const disabled = true;
const withdrawStyle = {display: "none"}

export class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.handleSelection = this.handleSelection.bind(this);
    this.clearItems = this.clearItems.bind(this);
    this.buy = this.buy.bind(this);
    this.update = this.update.bind(this);
    this.changeActive = this.changeActive.bind(this);
    this.initContract = this.initContract.bind(this);
    this.initPurchase = this.initPurchase.bind(this);
    this.initUpdate = this.initUpdate.bind(this);
    this.withdrawFunds = this.withdrawFunds.bind(this);
    this.setContractOwner = this.setContractOwner.bind(this);
    this.availabilityCheck = this.availabilityCheck.bind(this);
    this.statusCheck = this.statusCheck.bind(this);
    this.removeStatusCheck = this.removeStatusCheck.bind(this);
    this.checkReceipt = this.checkReceipt.bind(this);
    this.getReceipt = this.getReceipt.bind(this);
    this.disabledManageAdsMessage = this.disabledManageAdsMessage.bind(this);
    this.enabledManageAdsMessage = this.enabledManageAdsMessage.bind(this);

    this.state = {
      images: [],
      selectedItems: [],
      selectedImage: null,
      title: "",
      link: "",
      image: "",
      x: null,
      y: null,
      width: null,
      height: null,
      ipfsHash: null,
      buttonText: buttonText,
      clearText: clearText,
      buyButtonClass: buyButtonClass,
      disabled: disabled,
      web3: null,
      hundredEtherWall: null,
      style: null,
      withdrawStyle: withdrawStyle,
      contractOwner: null,
      network: null,
      networkLink: null,
      isHide: false,
      isActive: null,
      loading: true,
      intervalId: null,
      txHash: null,
      buyCheck: false,
      updateCheck: false,
      activeCheck: false,
      manageAdsDisabled: false,
      accountInterval: null,
      soldBlocks: 0,
    };
  }

  render() {
    if (this.state.loading) {
      return (
        <div onDoubleClick={handleCloseAll}>
          <Header
            network={this.state.network}
            networkLink={this.state.networkLink}
            withdraw={this.withdrawFunds}
            style={this.state.withdrawStyle}
            soldBlocks={this.state.soldBlocks}
          />
          <SelectableGrid
            clearItems={this.clearItems}
            network={this.state.network}
            images={this.state.images}
            items={items}
            selectedItems={this.state.selectedItems}
            handleSelection={this.handleSelection}
            web3={this.state.web3}
          />
        </div>
      );
    }
    return (
      <div className="homeWrapper" onDoubleClick={handleCloseAll}>
        <Header
          network={this.state.network}
          networkLink={this.state.networkLink}
          withdraw={this.withdrawFunds}
          style={this.state.withdrawStyle}
          soldBlocks={this.state.soldBlocks}
        />
        <SelectableGrid
          clearItems={this.clearItems}
          network={this.state.network}
          images={this.state.images}
          items={items}
          selectedItems={this.state.selectedItems}
          handleSelection={this.handleSelection}
          web3={this.state.web3}
        />
        <Web3Provider>
          <div className="bottomBar">
            <Button
              bsStyle="info"
              style={this.state.style}
              className="btn-left"
              onClick={this.clearItems}
              disabled={this.state.disabled}
            >
              {this.state.clearText}
            </Button>
            <Button
              className="btn-middle"
              bsStyle="default"
              onClick={!this.state.manageAdsDisabled ? () => scrollToComponent(this.refs.manage) : this.manageAdsDisabled}
              disabled={this.state.manageAdsDisabled}
            >
              {strings.manageAdsButtonText}
            </Button>
            <BuyModal
              className={this.state.buyButtonClass}
              buttonText={this.state.buttonText}
              selectedItems={this.state.selectedItems}
              disabled={this.state.disabled}
              buy={this.buy}
              style={this.state.style}
            />
          </div>
          <div>
            <ManageAdsForm
              disabledMessage={this.disabledManageAdsMessage}
              enabledMessage={this.enabledManageAdsMessage}
              ref={"manage"}
              contractOwner={this.state.contractOwner}
              update={this.update}
              changeActive={this.changeActive}
              web3={this.state.web3}
              images={this.state.images}
            />
          </div>
        </Web3Provider>
      </div>
    );
  }

  //disabling manage ad ManageAdsForm

  disabledManageAdsMessage() {
    this.setState({
      manageAdsDisabled: true,
    });
  }

  enabledManageAdsMessage() {
    this.setState({
      manageAdsDisabled: false,
    });
  }

  manageAdsDisabled() {
    handleErrorMessage(strings.messageNoAds);
  }

  //Grid Logic---------------------------------------------------------------------------------------------------------------
  //The following methods are used to handle the selection of the grid and initialize a purchase/update
  //NOTE: This does not include any web3 or smart contract logic
  //Grid Logic---------------------------------------------------------------------------------------------------------------

  clearItems(e) {
    this.setState({
      selectedItems: [],
      buttonText: buttonText,
      clearText: clearText,
      buyButtonClass: buyButtonClass,
      disabled: disabled,
    });
  }

  handleSelection (selectedItems) {
    this.setState({ selectedItems });

    if (selectedItems.length > 0 && this.availabilityCheck()) {

      let buttonText = "Buy " + calculateSize(this.state.selectedItems)[0] + " x " + calculateSize(this.state.selectedItems)[1]

      this.setState({
        buttonText: buttonText,
        clearText: "Clear " + calculateSize(this.state.selectedItems)[0] + " x " + calculateSize(this.state.selectedItems)[1],
        buyButtonClass: "btn-right-animation",
        disabled: false,
      });
    } else {
      this.clearItems(event);
      handleErrorMessage(strings.messageSpotTaken);
    }

  }

  buy(title, link, ipfsHash) {
    let coords = calculateCoords(0, this.state.selectedItems);
    let size = calculateSize(this.state.selectedItems);

    let images = this.state.images.slice();

    let idx = images[0] ? images[images.length - 1].idx + 1 : 0;

    let newImage = {
      idx: idx,
      owner: this.state.web3.eth.accounts[0],
      title: title,
      link: link,
      x: coords[0],
      y: coords[1],
      width: size[0],
      height: size[1],
      preview: true,
      ipfsHash: ipfsHash,
      active: true,
    }

    images.push(newImage)

    this.setState({
      images: images,
      title: title,
      link: link,
      x: coords[0],
      y: coords[1],
      width: size[0],
      height: size[1],
      ipfsHash: ipfsHash,
    });

    this.initPurchase();

    this.clearItems();
  }

  update(title, link, ipfsHash, selectedImage) {
    this.setState({
      title: title,
      link: link,
      ipfsHash: ipfsHash,
      selectedImage: selectedImage,
    });

    this.initUpdate();
  }

  changeActive(active, selectedImage) {
    this.setState({
      isActive: active,
      selectedImage: selectedImage,
    });
    this.initChangeActive(active, selectedImage);
  }

  availabilityCheck () {
    for(let i = 0; i < this.state.images.length; i++) {
      let x = this.state.images[i].x;
      let y = this.state.images[i].y;
      let width = this.state.images[i].width;
      let height = this.state.images[i].height;
      let blocks = calculateBlockPos(x, y, width, height);
      for(let j = 0; j < blocks.length; j++) {
        for(let h = 0; h < this.state.selectedItems.length; h++) {
          if (blocks[j] === this.state.selectedItems[h]){
            return false;
          }
        }
      }
    }
    return true;
  }

//web3 Logic---------------------------------------------------------------------------------------------------------------
//The following methods are used to get web3 and serup the contract
//web3 Logic---------------------------------------------------------------------------------------------------------------

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.
    getWeb3
    .then(results => {
      this.setState({
        web3: results.web3,
      });
      console.log(this.state.web3.currentProvider);
      console.log("web3 is connected: " + this.state.web3.isConnected());

      new Promise((resolve) => {
        let network = checkNetwork(this.state.web3.version.network);
        this.setState({
          network: network[0],
          networkLink: network[1],
        });
        this.initContract();
        this.setContractOwner();
        this.catchAccount();
        if (!this.state.web3.eth.accounts[0]) {
          handleErrorMessage(strings.messageUnlockWallet);
        }
        if (this.state.network === "Unknown" || this.state.network === "Main" || this.state.network === "Rinkeby") {
          this.loadData().then(() => {
            resolve();
          }).then(() => {
            let soldBlocks = 0;
            for (let i = 0; i < this.state.images.length; i++) {
              soldBlocks += ((this.state.images[i].width / 25) * (this.state.images[i].height / 25))
            }
            this.setState({
              soldBlocks: soldBlocks
            });
          })
        }
      })
      .then(() => {
        this.setState({
          loading: false
        });
      });

    })
    .catch(() => {
      console.log('Error finding web3.')
    });
  }

  initContract() {
    const hundredEtherWall = Contract(HundredEtherWallContract)

    hundredEtherWall.setProvider(this.state.web3.currentProvider);

    this.setState({
      hundredEtherWall: hundredEtherWall
    });
  }

  validateUrl(link) {
    let substring = "https://";
    let substring2 = "http://";

    if (link.includes(substring) || link.includes(substring2)) {
      return true;
    } else {
      return false;
    }
  }

  //Contract logic---------------------------------------------------------------------------------------------------------------
  //The following methods are used to call the methods in the smart contract
  //Contract Logic---------------------------------------------------------------------------------------------------------------

  //get ads from the blockchain and build up an array of ads in the state.
  loadData() {
    return new Promise((resolve) => {
      this.state.hundredEtherWall.deployed().then((hundredEtherWallInstance) => {
        hundredEtherWallInstance.getAds().then((result) => {
          const num = result.toNumber();
          let promises = [];
          for (let i = 0; i < num; i++) {
            let promise = hundredEtherWallInstance.ads(i).then((result) => {

              let link = "#";
              if (this.validateUrl(result[6])) {
                link = result[6];
              }

              let newImage = {
                idx: i,
                owner: result[0],
                title: result[5],
                link: link,
                ipfsHash: result[7],
                x: result[1].toNumber(),
                y: result[2].toNumber(),
                width: result[3].toNumber(),
                height: result[4].toNumber(),
                preview: false,
                forSale: result[8],
                active: result[9],
                marketPrice: result[10],
              }

              let images = this.state.images.slice();
              images.push(newImage)

              this.setState({
                images: images,
              });
            });
            promises.push(promise);
          }
          Promise.all(promises).then(() => {
            resolve();
          })
        });
      }).catch((error => {
        handleErrorMessage(strings.messageNotDeployed);
      }));
    });
  }

  initPurchase(e) {
    this.state.hundredEtherWall.deployed().then((hundredEtherWallInstance) => {
      let price = this.state.width * this.state.height * weiPixelPrice;
      hundredEtherWallInstance.buy.sendTransaction(this.state.x, this.state.y, this.state.width,
        this.state.height, this.state.title, this.state.link, this.state.ipfsHash,
        {from: this.state.web3.eth.accounts[0], gasPrice: highGasPrice, value: this.state.web3.toWei(price, 'wei')})
        .then((result => {
        this.setState({
          txHash: result,
          buyCheck: true,
        });
        this.statusCheck();
        handleSuccessMessage(strings.messagePurchaseReq);
      })).catch((error) => {
        handleErrorMessage(strings.messagePurchaseDec);
      });
    })
  }

  initUpdate(e) {
    this.state.hundredEtherWall.deployed().then((hundredEtherWallInstance) => {
      hundredEtherWallInstance.update.sendTransaction(this.state.selectedImage.idx, this.state.title,
        this.state.link, this.state.ipfsHash, {from: this.state.web3.eth.accounts[0], gas: highGasLimit, gasPrice: midGasPrice})
        .then((result => {
          this.setState({
            txHash: result,
            updateCheck: true,
          });
          this.statusCheck();
          handleSuccessMessage(strings.messageUpdateReq);
        })).catch((error) => {
          handleErrorMessage(strings.messageUpdateDec);
        });
    })
  }

  initChangeActive(active, selectedImage) {
    this.state.hundredEtherWall.deployed().then((hundredEtherWallInstance) => {
      hundredEtherWallInstance.setActive.sendTransaction(selectedImage.idx, active,
        {from: this.state.web3.eth.accounts[0], gas: lowGasLimit, gasPrice: lowGasPrice})
        .then((result => {
          this.setState({
            txHash: result,
            activeCheck: true,
          });
          this.statusCheck();
          handleSuccessMessage(strings.messageActionReq);
        })).catch((error) => {
          handleErrorMessage(strings.messageActionDec);
        });
    })
  }

  withdrawFunds(e) {
    this.state.hundredEtherWall.deployed().then((hundredEtherWallInstance) => {
      hundredEtherWallInstance.withdraw({from: this.state.web3.eth.accounts[0], gas: lowGasLimit, gasPrice: lowGasPrice})
      .then((result => {
        this.setState({
          txHash: result.tx,
        });
        this.statusCheck();
        handleSuccessMessage(strings.messageWithdrawReq);
      })).catch((error) => {
        handleErrorMessage(strings.messageWithdrawDec);
      });
    });
  }

  setContractOwner() {
    this.state.web3.eth.getAccounts((error, accounts) => {
      this.state.hundredEtherWall.deployed().then((hundredEtherWallInstance) => {
        hundredEtherWallInstance.contractOwner.call().then((contractOwner) => {
          if (accounts[0] === contractOwner) {
            this.setState({
              withdrawStyle: null,
              contractOwner: contractOwner,
            })
          } else {
            this.setState({
              withdrawStyle: withdrawStyle,
            })
          }
        });
      }).catch((error => {
        //
      }));
    });
  }

  //the following 4 methods are for checking if the transaction is mined. if so,
  //display messages and ajust the state so that the interface will update
  statusCheck() {
    let intervalId = setInterval(this.getReceipt, 1000);
    this.setState({intervalId: intervalId});
  }

  getReceipt() {
    this.state.web3.eth.getTransactionReceipt(this.state.txHash, this.checkReceipt);
  }

  checkReceipt(err, receipt) {
    if (receipt && receipt.status) {
      handleSuccessMessage(strings.messageTxComplete);

      if (this.state.buyCheck) {
        let image = this.state.images.pop();
        let images = this.state.images.slice();
        let soldBlocks = this.state.soldBlocks;

        image.preview = false;
        soldBlocks += ((image.width / 25) * (image.height / 25))

        images.push(image)

        this.setState({
          images: images,
          soldBlocks: soldBlocks
        });
        this.refs.manage.displayCheck();
      }

      if (this.state.updateCheck) {
        let images = this.state.images.slice();

        let image = images[this.state.selectedImage.idx];

        image.title = this.state.title;
        image.link = this.state.link;
        image.ipfsHash = this.state.ipfsHash

        images.splice(this.state.selectedImage.idx, 1, image);

        this.setState({
          images: images,
        });
      }

      if (this.state.activeCheck) {
        let images = this.state.images.slice();

        let image = images[this.state.selectedImage.idx];

        image.active = this.state.isActive;

        images.splice(this.state.selectedImage.idx, 1, image);

        this.setState({
          images: images,
        });
      }
      this.removeStatusCheck();
    }
  }

  removeStatusCheck() {
    if (this.state.intervalId) {
      clearInterval(this.state.intervalId);
    }
    this.setState({
      buyCheck: false,
      updateCheck: false,
      activeCheck: false
    });
  }

  // the following 2 methods are for checking the recent account. if the account changes,
  //the browser will force an hard realod
  catchAccount() {
    let account = this.state.web3.eth.accounts[0];
    let accountInterval = setInterval( () => {
      if (this.state.web3.eth.accounts[0] !== account) {
        account = this.state.web3.eth.accounts[0];
        window.location.reload();
      }
    }, 100);
    this.setState({accountInterval: accountInterval});
  }

  removeAccountCheck() {
    if (this.state.accountInterval) {
      clearInterval(this.state.accountInterval);
    }
  }

  //this is required for react in order to reset the interval we set for account and transaction cheching
  componentWillUnmount() {
    this.removeStatusCheck();
    this.removeAccountCheck();
  }
}

export default HomePage;
