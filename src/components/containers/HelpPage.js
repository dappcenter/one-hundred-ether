import React from 'react';
import '../../styles/about-page.css';
import * as strings from '../../config/strings';

const HelpPage = () => {
  return (
    <div className="wrapper">
      <h2>{strings.helpTitle}</h2>
      <h4>{strings.purchaseTitle}</h4>
      <div className="vidblock">
        <iframe src="https://www.youtube.com/embed/h6j_UuStp8k" frameBorder="0" allowFullScreen></iframe>
      </div>
      <h4 className="changeAd">{strings.changeTitle}</h4>
      <div className="vidblock">
        <iframe src="https://www.youtube.com/embed/BGlsnVSMcc4" frameBorder="0" allowFullScreen></iframe>
      </div>
    </div>
  );
};

export default HelpPage;
