import React from 'react';
import MarketTable from '../MarketTable';
import ManageAdsSale from '../ManageAdsSale';
import '../../styles/marketplace.css';
import Contract from 'truffle-contract';
import HundredEtherWallContract from '../../../build/contracts/HundredEtherWall.json'
import getWeb3 from '../../utils/getWeb3';
import {handleErrorMessage, handleSuccessMessage} from '../Notification';
import {checkNetwork} from '../../utils/network';
import { RingLoader } from 'react-spinners';
import {weiPixelPrice, midGasPrice, lowGasPrice,
				highGasLimit, lowGasLimit} from '../../config/config';
import * as strings from '../../config/strings';

class MarketPage extends React.Component {

	constructor (props) {
    super(props);
    this.forSale = this.forSale.bind(this);
    this.cancelSale = this.cancelSale.bind(this);
    this.initForSale = this.initForSale.bind(this);
    this.marketBuy = this.marketBuy.bind(this);
    this.statusCheck = this.statusCheck.bind(this);
    this.removeStatusCheck = this.removeStatusCheck.bind(this);
    this.checkReceipt = this.checkReceipt.bind(this);
    this.getReceipt = this.getReceipt.bind(this);

    this.state = {
      images: [],
      selectedImage: null,
      web3: null,
      hundredEtherWall: null,
      contractOwner: null,
      network: null,
      networkLink: null,
      loading: true,
      intervalId: null,
      txHash: null,
			saleCheck: false,
			cancelSale: false,
			marketBuyCheck: false,
			accountInterval: null,
			newPrice: null,
			oldPrice: null,
    };
  }

	render() {
    if (this.state.loading) {
      return (
				<div className='sweet-loading'>
					<RingLoader
						color={'#65cdf1'}
						loading={this.state.loading}
					/>
				</div>
      );
    }
		return (
      <div className="main">
        <div className="titles">
          <h1 className="titlesItem">{strings.marketTitle}</h1>
          <h2 className="titlesItem" >{strings.marketUnderTitle}</h2>
        </div>
        <div>
          <ManageAdsSale
            web3={this.state.web3}
            images={this.state.images}
            forSale={this.forSale}
            cancelSale={this.cancelSale}
						ref="manageSale"
          />
        </div>
        <div>
          <MarketTable
            web3={this.state.web3}
            images={this.state.images}
            marketBuy={this.marketBuy}
          />
        </div>
      </div>
    );
	}

	//web3 Logic---------------------------------------------------------------------------------------------------------------
	//The following methods are used to get web3 and serup the contract
	//web3 Logic---------------------------------------------------------------------------------------------------------------

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.
    getWeb3
    .then(results => {
      this.setState({
        web3: results.web3
      });
      console.log(this.state.web3.currentProvider);
      console.log("web3 is connected: " + this.state.web3.isConnected());

      new Promise((resolve) => {
				let network = checkNetwork(this.state.web3.version.network);
        this.setState({
          network: network[0],
          networkLink: network[1],
        });
				this.initContract();
				this.catchAccount();
        if (!this.state.web3.eth.accounts[0]) {
          handleErrorMessage(strings.messageUnlockWallet)
        }
        if (this.state.network === "Unknown" || this.state.network === "Main" || this.state.network === "Rinkeby") {
          this.loadData().then(() => {
            resolve();
          })
        }
      })
      .then(() => {
        this.setState({
          loading: false
        })
      });

    })
    .catch(() => {
      console.log('Error finding web3.')
    });
  }

  initContract() {
    const hundredEtherWall = Contract(HundredEtherWallContract)

    hundredEtherWall.setProvider(this.state.web3.currentProvider);

    this.setState({
      hundredEtherWall: hundredEtherWall
    });
  }

	//Contract logic---------------------------------------------------------------------------------------------------------------
	//The following methods are used to call the methods in the smart contract
	//Contract Logic---------------------------------------------------------------------------------------------------------------

	//get ads from the blockchain and build up an array of ads in the state.
  loadData() {
    return new Promise((resolve) => {
      this.state.hundredEtherWall.deployed().then((hundredEtherWallInstance) => {
        hundredEtherWallInstance.getAds().then((result) => {
          const num = result.toNumber();
          let promises = [];
          for (let i = 0; i < num; i++) {
            let promise = hundredEtherWallInstance.ads(i).then((result) => {

              let newImage = {
                idx: i,
                owner: result[0],
                title: result[5],
                link: result[6],
                ipfsHash: result[7],
                x: result[1].toNumber(),
                y: result[2].toNumber(),
                width: result[3].toNumber(),
                height: result[4].toNumber(),
                preview: false,
                forSale: result[8],
								active: result[9],
								marketPrice: result[10],
              }

              let images = this.state.images.slice();
              images.push(newImage)

              this.setState({
                images: images,
              });
            });
            promises.push(promise);
          }
          Promise.all(promises).then(() => {
            resolve();
          })
        });
      }).catch((error => {
        handleErrorMessage(strings.messageNotDeployed);
      }));
    });
  }

	//the following methods are used to set the ad for sale
  forSale(selectedImage, newPrice) {
    this.setState({
      selectedImage: selectedImage,
    });

    this.initForSale(newPrice);
  }

  initForSale(newPrice) {
    this.state.hundredEtherWall.deployed().then((hundredEtherWallInstance) => {
      hundredEtherWallInstance.setSale.sendTransaction(this.state.selectedImage.idx, true, newPrice,
        {from: this.state.web3.eth.accounts[0], gas: lowGasLimit, gasPrice: lowGasPrice})
        .then((result => {
          this.setState({
            txHash: result,
						saleCheck: true,
						newPrice: newPrice,
          });
          this.statusCheck();
          handleSuccessMessage(strings.messageSaleReq);
        })).catch((error) => {
          handleErrorMessage(strings.messageSaleDec);
        });
    })
  }

	//the following methods are used to cancel a sale on the ad
  cancelSale(selectedImage) {
    this.setState({
      selectedImage: selectedImage,
			cancelSaleCheck: true,
    });

    this.initCancelSale();
  }

  initCancelSale(e) {
    this.state.hundredEtherWall.deployed().then((hundredEtherWallInstance) => {
			let oldPrice = this.state.selectedImage.width * this.state.selectedImage.height * weiPixelPrice;
      hundredEtherWallInstance.setSale.sendTransaction(this.state.selectedImage.idx, false, oldPrice,
        {from: this.state.web3.eth.accounts[0], gas: lowGasLimit, gasPrice:lowGasPrice})
        .then((result => {
          this.setState({
            txHash: result,
						oldPrice: oldPrice
          });
          this.statusCheck();
          handleSuccessMessage(strings.messageCancelSaleReq);
        })).catch((error) => {
          handleErrorMessage(strings.messageCancelSaleDec);
        });
    })
  }

	//the following methods are for buying an ad in the market
	marketBuy(selectedImage) {
		this.setState({
			selectedImage: selectedImage,
		});

		this.initMarketBuy();
	}

  initMarketBuy(e) {
    this.state.hundredEtherWall.deployed().then((hundredEtherWallInstance) => {
      hundredEtherWallInstance.marketBuy.sendTransaction(this.state.selectedImage.idx,
        {from: this.state.web3.eth.accounts[0], gas: highGasLimit, gasPrice:midGasPrice,
					value: this.state.web3.toWei(this.state.selectedImage.marketPrice, 'wei')})
        .then((result => {
          this.setState({
            txHash: result,
						marketBuyCheck: true,
          });
          this.statusCheck();
          handleSuccessMessage(strings.messagePurchaseReq);
        })).catch((error) => {
          handleErrorMessage(strings.messagePurchaseDec);
        });
    })
  }

	//the following methods are for checking if the transaction is mined. if so,
  //display messages and ajust the state so that the interface will update
  statusCheck() {
    let intervalId = setInterval(this.getReceipt, 1000);
    this.setState({intervalId: intervalId});
  }

  getReceipt() {
    this.state.web3.eth.getTransactionReceipt(this.state.txHash, this.checkReceipt);
  }

  checkReceipt(err, receipt) {
    if (receipt && receipt.status) {
      handleSuccessMessage(strings.messageTxComplete);

			if (this.state.saleCheck) {
				let images = this.state.images.slice();

				let image = images[this.state.selectedImage.idx];
				image.forSale = true;
				image.marketPrice = this.state.newPrice;

				images.splice(this.state.selectedImage.idx, 1, image);

				this.setState({
					images: images,
				});
			}

			if (this.state.cancelSaleCheck) {
				let images = this.state.images.slice();

				let image = images[this.state.selectedImage.idx];
				image.forSale = false;
				image.marketPrice = this.state.oldPrice;

				images.splice(this.state.selectedImage.idx, 1, image);

				this.setState({
					images: images,
				});
			}

			if (this.state.marketBuyCheck) {
				let images = this.state.images.slice();

				let image = images[this.state.selectedImage.idx];
				image.forSale = false;
				image.owner = this.state.web3.eth.accounts[0];

				images.splice(this.state.selectedImage.idx, 1, image);

				this.setState({
					images: images,
				});
				this.refs.manageSale.loadFirstItem();
			}

      this.removeStatusCheck();
    }
  }

  removeStatusCheck() {
    if (this.state.intervalId) {
      clearInterval(this.state.intervalId);
    }
		this.setState({
			saleCheck: false,
			cancelSaleCheck: false,
			marketBuyCheck: false,
		});
  }

	// the following 2 methods are for checking the recent account. if the account changes,
  //the browser will force an hard realod
	catchAccount() {
		let account = this.state.web3.eth.accounts[0];
		let accountInterval = setInterval( () => {
			if (this.state.web3.eth.accounts[0] !== account) {
				account = this.state.web3.eth.accounts[0];
				window.location.reload();
			}
		}, 100);
		this.setState({accountInterval: accountInterval});
	}

	removeAccountCheck() {
		if (this.state.accountInterval) {
			clearInterval(this.state.accountInterval);
		}
	}

	//this is required for react in order to reset the interval we set for account and transaction cheching
  componentWillUnmount() {
    this.removeStatusCheck();
		this.removeAccountCheck();
  }
}

export default MarketPage;
