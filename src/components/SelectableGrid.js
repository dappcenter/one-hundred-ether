import React from 'react';
import { SelectableGroup, createSelectable } from 'react-selectable';
import GridItem from './GridItem';
import '../styles/ad-grid.css';
import preview from '../../public/images/preview.png';
import sale from '../../public/images/sale2.png';
import networkSwitch from '../../public/images/networkSwitch.jpg';
import inactive from '../../public/images/inactive.jpg';
import * as strings from '../config/strings';
import ReactGA from 'react-ga';


ReactGA.initialize('UA-120149716-1');

const SelectableComponent = createSelectable(GridItem);

class SelectableGrid extends React.Component {

  render () {
    if (this.props.web3) {
      if (this.props.network === "Unknown" || this.props.network === "Main" || this.props.network === "Rinkeby") {
        return (
          <SelectableGroup onSelection={this.props.handleSelection}>
            <div className="grid-container" onDoubleClick={this.props.clearItems}>
              {this.props.items.map((item) => {
                let selected = this.props.selectedItems.indexOf(item.id) > -1;
                return (
                  <SelectableComponent key={item.id} selected={selected} selectableKey={item.id} />
                );
              })}

              {this.props.images.map((image, i) => {
                const containerStyle = {
                  left: image.x,
                  top: image.y,
                  position: "absolute",
                }

                let previewStyle = {
                  position: "absolute",
                  top: 0,
                  left: 0,
                };

                if (!image.preview) {
                  previewStyle = {
                    display: "none",
                  }
                }

                let forSaleStyle = {
                  position: "absolute",
                  top: 0,
                  left: 0,
                  width: "20px",
                  height: "20px",
                };

                if (!image.forSale) {
                  forSaleStyle = {
                    display: "none",
                  }
                }

                return (
                  <ReactGA.OutboundLink className="imageContainer" key={i} eventLabel={image.active ? image.link : "#"} to={image.active ? image.link : "#"} style={containerStyle}>
                    <span className="title">
                      {image.active ? image.title : strings.imageInactive}
                    </span>
                    <img src={image.active ? "https://gateway.ipfs.io/ipfs/" + image.ipfsHash : inactive} width={image.width} height={image.height} role="presentation"/>
                    <img style={previewStyle} src={preview} width={image.width} height={image.height} role="presentation"/>
                    <img style={forSaleStyle} src={sale} role="presentation"/>
                  </ReactGA.OutboundLink>
                );
              })}
            </div>
          </SelectableGroup>
        );
      } else {
          return (
            <div className="grid-container-unsupported">
              <h3>
                {strings.unsupportedNetworkTitle}
              </h3>
              <h4>{strings.unsupportedNetworkExp}</h4>
              <a href="https://metamask.io" target="_blank" className="metamaskImg">
                <img src={networkSwitch} role="presentation"/>
              </a>
            </div>
          );
      }
    } else {
      return (
        <div className="grid-container-unsupported">
          <h3>
            {strings.unsupportedWeb3Exp}
          </h3>
          <a href="https://metamask.io" target="_blank" className="metamaskImg">
            <img src="https://raw.githubusercontent.com/MetaMask/faq/master/images/download-metamask.png" role="presentation"/>
          </a>
        </div>
      );
    }
  }

  // getGAData() {
  //   console.log(ReactGA);
  // }
  //
  // componentWillMount() {
  //   this.getGAData();
  // }
}

export default SelectableGrid;
