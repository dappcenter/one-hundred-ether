import React from 'react';
import {DropdownButton, MenuItem} from 'react-bootstrap';
import {Button} from 'react-bootstrap';
import sale from '../../public/images/sale2.png';
import * as strings from '../config/strings';

class ManageAdsSale extends React.Component {

  constructor (props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.loadMenuItems = this.loadMenuItems.bind(this);
    this.initiateSale = this.initiateSale.bind(this);
    this.initiateCancelSale = this.initiateCancelSale.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);

    this.state = {
      image: null,
      title: "",
      menuItems: [],
      newPrice: null,
      hasItems: false,
    };
  }

  render(){
    if (this.props.web3 && this.state.hasItems) {
      return (
        <div className="manageTotal">
          <div className="manageChild">
            <div className="wrap">
              <div className="saleItem">
                <label className="lab">{strings.manageSaleAdLabel}</label>
              </div>
                <DropdownButton
                  title={this.state.title.substring(0, 40)}
                  id="dropdown-size-med"
                  className="dropdown"
                >
                  {this.loadMenuItems()}
                </DropdownButton>
            </div>
            <div className="saleItem">
              <label className="lab">{strings.manageSalePriceLabel}</label>
              <input
                type="number"
                step="0.01"
                value={this.state.newPrice !== null ? this.state.newPrice : 0}
                onChange={this.handleInputChange}
                className="inputPrice"
              />
              <label className="lab">{strings.ethLabel}</label>
            </div>
            <div>
              <div className="saleItem">
                <Button
                  bsStyle="success"
                  className='buyButton submit'
                  disabled={this.state.image ? this.state.image.forSale : true}
                  onClick={this.initiateSale}
                >
                  {strings.marketSaleButton}
                </Button>
              </div>
              <div className="saleItem">
                <Button
                  bsStyle="danger"
                  className='buyButton submit'
                  disabled={this.state.image ? !this.state.image.forSale : true}
                  onClick={this.initiateCancelSale}
                >
                  {strings.marketCancelButton}
                </Button>
              </div>
            </div>
          </div>
          <div className="manageChild2">
            <div className="wrap">
              <table className="marketOverview">
                <tbody>
                  <tr>
                    <th>{strings.marketTablePosition}</th>
                    <td>{this.state.image.x} / {this.state.image.y}</td>
                  </tr>
                  <tr>
                    <th>{strings.marketTableSize}</th>
                    <td>{this.state.image.width}px / {this.state.image.height}px</td>
                  </tr>
                  <tr>
                    <th>{strings.marketTableSale}</th>
                    <td>{this.state.image.forSale ? <img role="presentation" src={sale} width="40px" height="30px"></img> : "No"}</td>
                  </tr>
                  <tr>
                    <th>{strings.marketTablePreview}</th>
                    <td>{<img src={"https://gateway.ipfs.io/ipfs/" + this.state.image.ipfsHash} role="presentation" width="40px" height="40px"></img>}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      );
    } else {
      return (null);
    }
  }

  loadMenuItems() {
    let menuItems = [];
    for (let i = 0; i < this.props.images.length; i++) {
      for (let x = 0; x < this.props.web3.eth.accounts.length; x++) {
        if (this.props.images[i].owner === this.props.web3.eth.accounts[x]) {
          let saleOrNot = this.props.images[i].forSale ? <img role="presentation" src={sale} width="40px" height="30px"></img> : "";
          menuItems.push(<MenuItem onClick={() => this.onClick(this.props.images[i])} key={i}>{this.props.images[i].title} {saleOrNot} </MenuItem>);
        }
      }
    }
    // this.setState({
    //   menuItems: menuItems
    // })
    return menuItems
  }

  loadFirstItem() {
    let selected = false;
    for (let i = 0; i < this.props.images.length; i++) {
      for (let x = 0; x < this.props.web3.eth.accounts.length; x++) {
        if (this.props.images[i].owner === this.props.web3.eth.accounts[x]) {
          if (!selected) {
            this.onClick(this.props.images[i]);
            selected = true;
            this.setState({
              newPrice: this.props.web3.fromWei(this.props.images[i].marketPrice, 'ether'),
              hasItems: true,
            })
          }
        }
      }
    }
  }

  onClick(image) {
    if (!image.forSale) {

      this.setState({
        image: image,
        title: image.title,
        newPrice: this.props.web3.fromWei(image.marketPrice, 'ether')
      });
    } else {
      this.setState({
        image: image,
        title: image.title + strings.dropdownForSale,
        newPrice: this.props.web3.fromWei(image.marketPrice, 'ether')
      });
    }
  }

  handleInputChange(e) {
    this.setState({
      newPrice: e.target.value,
    });
  }

  initiateSale() {
    let newPrice = this.props.web3.toWei(this.state.newPrice, 'ether');
    this.props.forSale(this.state.image, newPrice);
  }

  initiateCancelSale() {
    this.props.cancelSale(this.state.image);
  }

  componentWillMount() {
    this.loadFirstItem();
  }

}

export default ManageAdsSale;
