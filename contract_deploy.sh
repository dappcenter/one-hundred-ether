#!/usr/bin/env sh

#this script can be executed to deploy the smart contract to an ethereum network
#this can either be the mainnet, one of the testnets or the localhost net

NET=${NET:-development}

# compile contract
truffle compile \
	&& echo "contract compilation succesful"
	
#migrate contract to network
truffle migrate --network ${NET} \
	&& echo "contract migration succesful"

